#include<stdio.h>
#include<stdlib.h>

// Quizz : devinez ce que font ces fontions ci-dessous :

int quizz1(char *s) {
    char *p;
    int answer = 0;
    p = s;
    while (*p++) {
        answer++;
    }
    return answer;
}

int quizz2(char *s1, char *s2) {
    char *p, *q;
    p = s1;
    q = s2;
    while (*p && *q && *p++ == *q++);
    return *p == 0 && *q == 0;
}

int main() {
    char t1[] = "Bonjour";
    char t2[] = "Bonjour";
    char t3[] = "Au revoir";

    printf("Quizz1 %d\n", quizz1(t1));
    printf("Quizz1 %d\n", quizz1(t3));

    printf("Quizz2 %d\n", quizz2(t1, t1));
    printf("Quizz2 %d\n", quizz2(t1, t2));
    printf("Quizz2 %d\n", quizz2(t1, t3));

    exit(0);
}
