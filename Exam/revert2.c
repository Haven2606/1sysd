#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    char temp;
    for (int i = 1; i < argc; i++) {
        int len = strlen(argv[i]);
        for (int j = 0; j < len / 2; j++) {
            temp = argv[i][j];
            argv[i][j] = argv[i][len - j - 1];
            argv[i][len - j - 1] = temp;
        }
    }

    for (int i = argc - 1; i > 0; i--) {
        int len = strlen(argv[i]);
        for (int j = 0; j < len / 2; j++) {
            temp = argv[i][j];
            argv[i][j] = argv[i][len - j - 1];
            argv[i][len - j - 1] = temp;
        }
        printf("%s\n", argv[i]);
    }

    return 0;
}
