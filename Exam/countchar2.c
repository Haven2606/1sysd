#include <stdio.h>
#include <string.h>
#include <ctype.h>

int count_char(char *str, char ch, int case_insensitive) {
    int count = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (case_insensitive) {
            if (tolower(str[i]) == tolower(ch)) {
                count++;
            }
        } else {
            if (str[i] == ch) {
                count++;
            }
        }
    }
    return count;
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Usage: %s <character> <string>\n", argv[0]);
        return 1;
    }
    char *str = argv[argc-1];
    char ch = argv[argc-2][0];
    int case_insensitive = 0;

    if (argc > 3 && strcmp(argv[argc-3], "-i") == 0) {
        case_insensitive = 1;
    }

    printf("%d\n", count_char(str, ch, case_insensitive));

    return 0;
}
