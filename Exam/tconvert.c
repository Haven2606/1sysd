#include <stdio.h>
#include <stdlib.h>

float celsius2fahrenheit(float celsius) {
    return (9.0/5.0) * celsius + 32;
}

float fahrenheit2celsius(float fahrenheit) {
    return (fahrenheit - 32) * 5.0/9.0;
}

int main() {
    char choice;
    float num;
    
    printf("Voulez-vous convertir de Celsius a Fahrenheit (1) ou de Fahrenheit a Celsius (2)? ");
    scanf(" %c", &choice);
    
    if (choice == '1') {
        printf("Saisir la temperature en degres Celsius: ");
        scanf("%f", &num);
        printf("Le resultat en degres Fahrenheit est: %.2f°F\n", celsius2fahrenheit(num));
    } else if (choice == '2') {
        printf("Saisir la temperature en degres Fahrenheit: ");
        scanf("%f", &num);
        printf("Le resultat en degres Celsius est: %.2f°C\n", fahrenheit2celsius(num));
    } else {
        printf("Choix non valide");
    }
    
    return 0;
}
