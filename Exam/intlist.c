Le problème du programme est qu'une boucle est créée dans la liste chaînée. Cela se produit lorsque le dernier nœud de la liste (généralement avec un pointeur suivant nul) pointe vers un nœud précédent de la liste, créant une boucle infinie lors du parcours de la liste.

Dans ce cas, dans la fonction main, la ligne suivante crée une boucle en pointant le dernier nœud vers le deuxième nœud de la liste :
head->next->next->next->next = head->next;

Cela peut être résolu en s'assurant que le dernier nœud de la liste a un pointeur next nul.

Voici une implémentation de cet algorithme en C :

#include <stdio.h>
#include <stdlib.h>

typedef struct node node;
struct node {
    int val;
    node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list_slow(node *head) {
    node *walk = head;
    while (walk != NULL) {
        printf("%d ", walk->val);
        fflush(stdout);
        walk = walk->next;
        sleep(1);
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);

    if (head == NULL) {
        head = newnode;
    } else {
        walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
    }
    return head;
}

int detect_cycle(node *head) {
    node *tortoise = head;
    node *hare = head;

    while (hare != NULL && hare->next != NULL) {
        tortoise = tortoise->next;
        hare = hare->next->next;

        if (tortoise == hare) {
            // Boucle détectée
            return 1;
        }
    }

    // Pas de boucle détectée
    return 0;
}

int main() {
    node *head = NULL;

    head = append_val(head, 42);
    head = append_val(head, 12);
    head = append_val(head, -5);
    head = append_val(head, 41);
    print_list_slow(head);

    // Créer une boucle
    head->next->next->next->next = head->next;

    // Tester la détection de boucle
    if (detect_cycle(head)) {
        printf("La liste contient une boucle.\n");
    } else {
        printf("La liste ne contient pas de boucle.\n");
    }

    return 0;
}

