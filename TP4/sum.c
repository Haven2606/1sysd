#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    double value, sum = 0;

    for(int i = 0; i < argc; i++) {
        value = strtod(argv[i], NULL);
        sum += value;
    }
    printf("%.2lf\n", sum);
}
