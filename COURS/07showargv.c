#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    // argc = argument count ; combien d'argument j'ai reçu
    // argv = arguments values ; quels sont-ils ?
    // argv[0] : nom du programme exécuté
    // argv[1] : premier argument,
    // argv[2] : deuxième argument, etc.

    for (int i = 0; i < argc; i++) {
        printf("Argument %d : %s\n", i, argv[i]);
    }

    exit(0);

}
